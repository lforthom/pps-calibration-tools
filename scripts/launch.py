#!/bin/python
import subprocess
import argparse
import glob
import os
import re

def listInputs(directory, max_files=-1):
    i = 0
    out = dict()
    for f in glob.glob(args.json_dir):
        if '.json' in f:
            try:
                begin_run = int(re.findall('Run(?:\d+)', f)[0].replace('Run', ''))
                if args.json_dir[0] == '/': # absolute path given
                    out[begin_run] = os.path.relpath(f, os.environ['CMSSW_BASE']+'/src')
                else: # relative path ; must be within this CMSSW scope
                    out[begin_run] = os.path.relpath(os.getcwd(), os.environ['CMSSW_BASE']+'/src')+'/'+f
                i += 1
            except IndexError:
                print 'Skipped file', f
                continue
            if max_files > 0 and i >= max_files:
                break
    return out

def buildConfig(tmpl_filename, out_filename, to_replace=dict()):
    out = open(out_filename, 'w')
    for l in open(tmpl_filename):
        for k, v in to_replace.iteritems():
            if k in l:
                l = l.replace(k, v)
        out.write(l)

parser = argparse.ArgumentParser(description='Launch the production of SQLite files from JSONs')
parser.add_argument('-i', type=str, dest='json_dir', help='Location to the JSON input files', default='jsons')
parser.add_argument('--cfg', type=str, dest='cfg_template', help='Path to the template file', required=True)
parser.add_argument('--txt', type=str, dest='txt_template', help='CondDB metatada template file', required=True)
args = parser.parse_args()

files = listInputs(args.json_dir)
print files

#exit(0)
out_sqlite = 'ppsDiamond_timingCalibration.db'
out_metadata = 'ppsDiamond_timingCalibration.txt'
i = 0
for r, f in files.iteritems():
    out_cfg = 'writer_tmp_cfg.py'
    to_replace = {
        'XXX_INPUT_JSON_XXX': f,
        'XXX_OUTPUT_SQLITE_XXX': out_sqlite,
        'XXX_BEGIN_XXX': str(r),
        'XXX_TAG_XXX': 'PPSDiamondTimingCalibration',
    }
    buildConfig(args.cfg_template, out_cfg, to_replace)
    #launchJob(out_cfg, out_sqlite)
    subprocess.call(['cmsRun', out_cfg])
    subprocess.call(['rm', out_cfg])
    i += 1
print 'inserted', i, 'payloads into the sqlite file'
to_replace = {
    'XXX_RUN_BEGIN_XXX': 'null',
}
buildConfig(args.txt_template, out_metadata, to_replace)
#subprocess.call(['uploadConditions.py', sqlite_file, '-a', '.'])

