import FWCore.ParameterSet.Config as cms

process = cms.Process('test')

process.source = cms.Source('EmptySource')
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(1))

from CondFormats.CTPPSReadoutObjects.PPSTimingDetEnum_cff import PPSTimingDetEnum

# load calibrations from JSON file
process.load('CalibPPS.ESProducers.ppsTimingCalibrationESSource_cfi')
process.ppsTimingCalibrationESSource.calibrationFile = cms.FileInPath('XXX_INPUT_JSON_XXX')
process.ppsTimingCalibrationESSource.subDetector = PPSTimingDetEnum.PPS_DIAMOND

# output service for database
process.load('CondCore.CondDB.CondDB_cfi')
process.CondDB.connect = 'sqlite_file:XXX_OUTPUT_SQLITE_XXX' # SQLite output

process.PoolDBOutputService = cms.Service('PoolDBOutputService',
    process.CondDB,
    timetype = cms.untracked.string('runnumber'),
    toPut = cms.VPSet(
        cms.PSet(
            record = cms.string('PPSTimingCalibrationRcd'),
            tag = cms.string('XXX_TAG_XXX'),
        )
    )
)

process.load('CondTools.CTPPS.ppsTimingCalibrationWriter_cfi')
process.ppsTimingCalibrationWriter.fromRunIOV = cms.uint32(XXX_BEGIN_XXX)

process.path = cms.Path(
    process.ppsTimingCalibrationWriter
)

