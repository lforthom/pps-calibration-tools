# Scripts for PPS calibration

This repository collects a list of utilities to help the production and upload
of calibration parameters to CMS' `CondDB`.

Prerequisites
=============

This set of tools are initially developed for the `CMSSW_10_6_X` release cycle.
Currently, a minimal [collection of changes](https://github.com/cms-sw/cmssw/compare/master...forthommel:ppsTimingCalib_esLabels_andFixes) is required to patch the various `ESProducer`/`EDAnalyzer` modules invoked:

```sh
git cms-merge-topic forthommel:ppsTimingCalib_esLabels_andFixes
```

Example
=======

```sh
python scripts/launch.py \
  -i /afs/cern.ch/work/e/ebossini/Analysis/Calibration/CalibFiles/calib_20190413_Run*.json \
  --cfg templates/ppsTimingCalibrationWriter_cfg.py \
  --txt templates/ppsTimingCalibration_conddb.txt
```
