#ifndef TreeProducer_TreeEvent_h
#define TreeProducer_TreeEvent_h

#include "TTree.h"
#include <vector>

struct TreeEvent
{
  void clear() {
    run = 0;
    rechitArm.clear(); rechitPlane.clear(); rechitChannel.clear();
    rechitX.clear(); rechitY.clear(); rechitZ.clear();
    rechitT.clear(); rechitTRes.clear(); rechitToT.clear(); rechitOOT.clear();
    trackArm.clear(); trackNumHits.clear(); trackNumPlanes.clear();
    trackX.clear(); trackY.clear(); trackZ.clear();
    trackT.clear(); trackTRes.clear(); trackOOT.clear();
    pixtrkArm0 = pixtrkArm1 = 0;
  }
  void create( TTree* tree ) {
    tree->Branch( "run", &run, "run/i" );
    tree->Branch( "rechitArm", &rechitArm );
    tree->Branch( "rechitPlane", &rechitPlane );
    tree->Branch( "rechitChannel", &rechitChannel );
    tree->Branch( "rechitX", &rechitX );
    tree->Branch( "rechitY", &rechitY );
    tree->Branch( "rechitZ", &rechitZ );
    tree->Branch( "rechitT", &rechitT );
    tree->Branch( "rechitTRes", &rechitTRes );
    tree->Branch( "rechitToT", &rechitToT );
    tree->Branch( "rechitOOT", &rechitOOT );
    tree->Branch( "trackArm", &trackArm );
    tree->Branch( "trackNumHits", &trackNumHits );
    tree->Branch( "trackNumPlanes", &trackNumPlanes );
    tree->Branch( "trackX", &trackX );
    tree->Branch( "trackY", &trackY );
    tree->Branch( "trackZ", &trackZ );
    tree->Branch( "trackT", &trackT );
    tree->Branch( "trackTRes", &trackTRes );
    tree->Branch( "trackOOT", &trackOOT );
    tree->Branch( "pixtrkArm0", &pixtrkArm0, "pixtrkArm0/i" );
    tree->Branch( "pixtrkArm1", &pixtrkArm1, "pixtrkArm1/i" );
  }
  void attach( TTree* tree ) {
    tree->SetBranchAddress( "run", &run );
    tree->SetBranchAddress( "rechitArm", &rechitArm );
    tree->SetBranchAddress( "rechitPlane", &rechitPlane );
    tree->SetBranchAddress( "rechitChannel", &rechitChannel );
    tree->SetBranchAddress( "rechitX", &rechitX );
    tree->SetBranchAddress( "rechitY", &rechitY );
    tree->SetBranchAddress( "rechitZ", &rechitZ );
    tree->SetBranchAddress( "rechitT", &rechitT );
    tree->SetBranchAddress( "rechitTRes", &rechitTRes );
    tree->SetBranchAddress( "rechitToT", &rechitToT );
    tree->SetBranchAddress( "rechitOOT", &rechitOOT );
    tree->SetBranchAddress( "trackArm", &trackArm );
    tree->SetBranchAddress( "trackNumHits", &trackNumHits );
    tree->SetBranchAddress( "trackNumPlanes", &trackNumPlanes );
    tree->SetBranchAddress( "trackX", &trackX );
    tree->SetBranchAddress( "trackY", &trackY );
    tree->SetBranchAddress( "trackZ", &trackZ );
    tree->SetBranchAddress( "trackT", &trackT );
    tree->SetBranchAddress( "trackTRes", &trackTRes );
    tree->SetBranchAddress( "trackOOT", &trackOOT );
    tree->SetBranchAddress( "pixtrkArm0", &pixtrkArm0 );
    tree->SetBranchAddress( "pixtrkArm1", &pixtrkArm1 );
  }

  unsigned int run;
  std::vector<unsigned int> rechitArm, rechitPlane, rechitChannel;
  std::vector<double> rechitX, rechitY, rechitZ, rechitT, rechitTRes, rechitToT;
  std::vector<int> rechitOOT;
  std::vector<unsigned int> trackArm, trackNumHits, trackNumPlanes;
  std::vector<double> trackX, trackY, trackZ, trackT, trackTRes;
  std::vector<int> trackOOT;
  unsigned int pixtrkArm0, pixtrkArm1;
};

#endif

