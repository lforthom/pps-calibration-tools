#include <memory>

#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/one/EDAnalyzer.h"
#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/ServiceRegistry/interface/Service.h"
#include "FWCore/Utilities/interface/InputTag.h"

#include "CommonTools/UtilAlgos/interface/TFileService.h"

#include "DataFormats/Common/interface/DetSetVector.h"

#include "DataFormats/CTPPSDetId/interface/CTPPSDiamondDetId.h"
#include "DataFormats/CTPPSReco/interface/CTPPSDiamondRecHit.h"
#include "DataFormats/CTPPSReco/interface/CTPPSDiamondLocalTrack.h"
#include "DataFormats/CTPPSReco/interface/CTPPSPixelLocalTrack.h"

#include "../interface/TreeEvent.h"

#include "TTree.h"

class TreeProducer : public edm::one::EDAnalyzer<edm::one::SharedResources>
{
  public:
    explicit TreeProducer( const edm::ParameterSet& );

    static void fillDescriptions( edm::ConfigurationDescriptions& descriptions );

  private:
    virtual void beginJob() override {}
    virtual void analyze( const edm::Event&, const edm::EventSetup& ) override;
    virtual void endJob() override {}

    // ----------member data ---------------------------
    edm::EDGetTokenT<edm::DetSetVector<CTPPSDiamondRecHit> > rechitsToken_;
    edm::EDGetTokenT<edm::DetSetVector<CTPPSDiamondLocalTrack> > tracksToken_;
    edm::EDGetTokenT<edm::DetSetVector<CTPPSPixelLocalTrack> > pixTracksToken_;

    TTree* tree_;
    TreeEvent evt_;
};

TreeProducer::TreeProducer( const edm::ParameterSet& iConfig ) :
  rechitsToken_  ( consumes<edm::DetSetVector<CTPPSDiamondRecHit> >    ( iConfig.getParameter<edm::InputTag>( "rechitsTag" ) ) ),
  tracksToken_   ( consumes<edm::DetSetVector<CTPPSDiamondLocalTrack> >( iConfig.getParameter<edm::InputTag>( "tracksTag" ) ) ),
  pixTracksToken_( consumes<edm::DetSetVector<CTPPSPixelLocalTrack> >  ( iConfig.getParameter<edm::InputTag>( "pixTracksTag" ) ) )
{
  usesResource( "TFileService" );
  edm::Service<TFileService> fs;
  tree_ = fs->make<TTree>( "ntp", "ntuple" );
  evt_.create( tree_ );
}

void
TreeProducer::analyze( const edm::Event& iEvent, const edm::EventSetup& iSetup )
{
  evt_.clear();
  evt_.run = iEvent.id().run();
  for ( const auto& ds_hit : iEvent.get( rechitsToken_ ) ) {
    const CTPPSDiamondDetId detid( ds_hit.detId() );
    for ( const auto& hit : ds_hit ) {
      evt_.rechitArm.emplace_back( detid.arm() );
      evt_.rechitPlane.emplace_back( detid.plane() );
      evt_.rechitChannel.emplace_back( detid.channel() );
      evt_.rechitX.emplace_back( hit.getX() );
      evt_.rechitY.emplace_back( hit.getY() );
      evt_.rechitZ.emplace_back( hit.getZ() );
      evt_.rechitT.emplace_back( hit.getT() );
      evt_.rechitTRes.emplace_back( hit.getTPrecision() );
      evt_.rechitToT.emplace_back( hit.getToT() );
      evt_.rechitOOT.emplace_back( hit.getOOTIndex() );
    }
  }
  for ( const auto& ds_track : iEvent.get( tracksToken_ ) ) {
    const CTPPSDiamondDetId detid( ds_track.detId() );
    for ( const auto& track : ds_track ) {
      evt_.trackArm.emplace_back( detid.arm() );
      evt_.trackNumHits.emplace_back( track.getNumOfHits() );
      evt_.trackNumPlanes.emplace_back( track.getNumOfPlanes() );
      evt_.trackX.emplace_back( track.getX0() );
      evt_.trackY.emplace_back( track.getY0() );
      evt_.trackZ.emplace_back( track.getZ0() );
      evt_.trackT.emplace_back( track.getT() );
      evt_.trackTRes.emplace_back( track.getTSigma() );
      evt_.trackOOT.emplace_back( track.getOOTIndex() );
    }
  }
  for ( const auto& ds_track : iEvent.get( pixTracksToken_ ) ) {
    const CTPPSDetId detid( ds_track.detId() );
    if ( detid.arm() == 0 )
      evt_.pixtrkArm0 += ds_track.size();
    else if ( detid.arm() == 1 )
      evt_.pixtrkArm1 += ds_track.size();
  }

  //--- only fill if at least a rechit or a track is present
  if ( !evt_.trackArm.empty() && !evt_.rechitArm.empty() )
    tree_->Fill();
}

void
TreeProducer::fillDescriptions( edm::ConfigurationDescriptions& descriptions )
{
  edm::ParameterSetDescription desc;
  desc.add<edm::InputTag>( "rechitsTag", edm::InputTag( "ctppsDiamondRecHits" ) );
  desc.add<edm::InputTag>( "tracksTag", edm::InputTag( "ctppsDiamondLocalTracks" ) );
  desc.add<edm::InputTag>( "pixTracksTag", edm::InputTag( "ctppsPixelLocalTracks" ) );
  descriptions.add( "treeProducer", desc );
}

//define this as a plug-in
DEFINE_FWK_MODULE( TreeProducer );

